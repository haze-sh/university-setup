import shutil
from pathlib import Path
import os
import sys

# We read a configuration file for university setup that is located
# in $XDG_CONFIG_HOME/university-setup/config.cfg


def get_default_file(name: str) -> Path:
    # System installation
    f1 = Path('/etc/opt/mkessler/university-setup') / name
    # no installation, try relative path
    f2 = Path(__file__).parent.parent / 'config' / name
    if f1.exists():
        return f1
    if f2.exists():
        return f2
    raise FileNotFoundError(f'Default file {name} not found, bad installation.')


# Ensure that config.py and fallback.yaml are present
# in the config directory and returns this directory
def get_config_dir() -> Path:
    if 'XDG_CONFIG_HOME' in os.environ.keys():
        xdg_config_home = Path(os.environ['XDG_CONFIG_HOME']).resolve()
    else:
        xdg_config_home = Path('~/.config').expanduser().resolve()
    config_file = xdg_config_home / 'university-setup' / 'config.py'
    fallback_file = xdg_config_home / 'university-setup' / 'fallback.yaml'
    config_file.parent.mkdir(exist_ok=True, parents=True)

    # Copy defaults if not present already
    if not config_file.exists():
        shutil.copy(get_default_file('config.py'), config_file)
        print(f'Initialized default config file at {str(config_file)}.')
    if not fallback_file.exists():
        shutil.copy(get_default_file('fallback.yaml'), fallback_file)
        print(f'Initialized default fallback file at {str(fallback_file)}.')

    return config_file.parent.absolute()


FALLBACK_COURSE_INFO_FILE = get_config_dir() / 'fallback.yaml'

sys.path.append(str(get_config_dir()))

# Note that IDEs will probably complain about this, since they cannot find the module right now
# they might also flag this as an unused import, but the imported config values
# are in turn imported by all the oder scripts
from config import *

# future: potentially check that config in fact defines all symbols that we need
