PKGNAME=university-setup
PREFIX=mkessler

install:
	mkdir -p $(DESTDIR)/opt/$(PREFIX)/${PKGNAME}
	mkdir -p $(DESTDIR)/etc/opt/$(PREFIX)/${PKGNAME}
	
	cp src/* $(DESTDIR)/opt/$(PREFIX)/${PKGNAME}
	cp config/* $(DESTDIR)/etc/opt/$(PREFIX)/${PKGNAME}
	
	install -Dm644 "LICENSE" $(DESTDIR)/usr/share/licenses/$(PREFIX)/${PKGNAME}/LICENSE

uninstall:
	rm -rf $(DESTDIR)/opt/$(PREFIX)/${PKGNAME}
	rm -rf $(DESTDIR)/etc/opt/$(PREFIX)/${PKGNAME}
	rm -rf $(DESTDIR)/usr/share//licenses/$(PREFIX)/${PKGNAME}
